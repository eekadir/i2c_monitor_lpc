/*
 * @brief I2C bus slave example using interrupt mode
 *
 * @note
 * Copyright(C) NXP Semiconductors, 2015
 * All rights reserved.
 *
 * @par
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * LPC products.  This software is supplied "AS IS" without any warranties of
 * any kind, and NXP Semiconductors and its licensor disclaim any and
 * all warranties, express or implied, including all implied warranties of
 * merchantability, fitness for a particular purpose and non-infringement of
 * intellectual property rights.  NXP Semiconductors assumes no responsibility
 * or liability for the use of the software, conveys no license or rights under any
 * patent, copyright, mask work right, or any other intellectual property rights in
 * or to any products. NXP Semiconductors reserves the right to make changes
 * in the software without notification. NXP Semiconductors also makes no
 * representation or warranty that such application will be suitable for the
 * specified use without further testing or modification.
 *
 * @par
 * Permission to use, copy, modify, and distribute this software and its
 * documentation is hereby granted, under NXP Semiconductors' and its
 * licensor's relevant copyrights in the software, without fee, provided that it
 * is used in conjunction with NXP Semiconductors microcontrollers.  This
 * copyright, permission, and disclaimer notice must appear in all copies of
 * this code.
 */
#include "string.h"
#include "board.h"
#include "i2cmonitor.h"
/** @defgroup PERIPH_I2CSINT_5411X I2C bus slave example using interrupt mode
 * @ingroup EXAMPLES_PERIPH_5411X
 * @include "periph_i2cs_interrupt\readme.txt"
 */
#define I2C1_FLEXCOMM   1
/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

uint8_t printbuff[256];
uint8_t MonData[256];
I2C_MONITOR_T i2cMon = { MonData,sizeof(MonData),0,0,0,0};

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/* Initializes pin muxing for I2C interface - note that SystemInit() may
   already setup your pin muxing at system startup */
static void Init_I2C_PinMux(void)
{
#if defined(BOARD_NXP_LPCXPRESSO_54114)
	/* Connect the I2C1_SDA and I2C1_SCL signals to port pins */
	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 23, (IOCON_FUNC1 | IOCON_DIGITAL_EN));
	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 24, (IOCON_FUNC1 | IOCON_DIGITAL_EN));

#else
	/* Configure your own I2C pin muxing here if needed */
#warning "No I2C pin muxing defined"
#endif
}

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/**
 * @brief	Main routine for I2C example
 * @return	Function should not exit
 */
int main(void)
{
	int loop = 1;	/* Used to fix the unreachable statement warning */

	/* Generic Initialization */
	SystemCoreClockUpdate();
	Board_Init();

	/* Clear activity LED */
	Board_LED_Set(0, false);

	/* Setup I2C pin muxing */
	Init_I2C_PinMux();

	i2cmonitor_Start(&i2cMon, LPC_I2C1);

	DEBUGOUT("I2C Monitorin Sample\n");

	while (loop) {

		if(i2cMon.DataReady)
		{
			memcpy(printbuff,i2cMon.pRxBuffer,i2cMon.Len);
			printbuff[i2cMon.Len]=0;
			i2cMon.DataReady = 0;
			i2cMon.Len = 0;
			DEBUGOUT("%s",printbuff);

		}
	}
	return 0;
}
