/*
 * i2cmonitor.h
 *
 *  Created on: Jul 8, 2019
 *      Author: gadi
 */
#include "chip.h"

#ifndef I2CMONITOR_H_
#define I2CMONITOR_H_

static INLINE uint32_t i2c_GetPendingInt(LPC_I2C_T *pI2C)
{
	return pI2C->INTSTAT;
}

static INLINE uint32_t i2c_GetStatus(LPC_I2C_T *pI2C)
{
	return pI2C->STAT;
}

static INLINE void i2c_ClearStatus(LPC_I2C_T *pI2C, uint32_t stats)
{
	pI2C->STAT |= (stats & (I2C_STAT_MONOV | I2C_STAT_MONIDLE)); //only this bits can clear by software
}

__STATIC_INLINE void i2c_MonitorEnable(LPC_I2C_T *pI2C)
{
	pI2C->CFG = (pI2C->CFG & I2C_CFG_MASK) | I2C_CFG_MONEN;
}

__STATIC_INLINE void i2c_MonitorDisable(LPC_I2C_T *pI2C)
{
	pI2C->CFG = (pI2C->CFG & I2C_CFG_MASK) & ~I2C_CFG_MONEN;
}

__STATIC_INLINE uint32_t i2c_GetMonitorRxData(LPC_I2C_T *pI2C)
{
	return pI2C->MONRXDAT;
}

typedef struct
{
	uint8_t* pRxBuffer;
	const uint32_t Size;
	volatile uint32_t Index;
	volatile uint32_t Len;
	volatile uint8_t DataReady;
	volatile uint8_t Error;
}I2C_MONITOR_T;


void i2cmonitor_Start(I2C_MONITOR_T* mntr, LPC_I2C_T* i2c);

#endif /* I2CMONITOR_H_ */
