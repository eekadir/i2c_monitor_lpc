/*
 * i2cmonitor.c
 *
 *  Created on: Jul 8, 2019
 *      Author: gadi
 */

#include "board.h"
#include "i2cmonitor.h"

#define I2C1_FLEXCOMM   1

#define CH_START	's'
#define CH_RESTART	'r'
#define CH_ACK		' '
#define CH_NACK		'.'
#define CH_STOP		'\n'



I2C_MONITOR_T* Monitor1 ;

const uint8_t HexTable[17] = {"0123456789ABCDEF"};

/*************************************
 *  Private Functions
 *************************************/

static INLINE void i2cMonitor_PutStart(I2C_MONITOR_T* mntr)
{
	mntr->Index = 0;
	mntr->pRxBuffer[mntr->Index++] = CH_START;
}

static INLINE void i2cMonitor_PutRestart(I2C_MONITOR_T* mntr)
{
	mntr->pRxBuffer[mntr->Index++] = CH_RESTART;
}

static INLINE void i2cMonitor_PutData(I2C_MONITOR_T* mntr, uint8_t b)
{
	mntr->pRxBuffer[mntr->Index++] = HexTable[b>>4];
	mntr->pRxBuffer[mntr->Index++] = HexTable[b&0xF];
}

static INLINE void i2cMonitor_PutACK(I2C_MONITOR_T* mntr,uint8_t b)
{
	mntr->pRxBuffer[mntr->Index] = b;
	mntr->Index++;
}

static INLINE void i2cMonitor_PutStop(I2C_MONITOR_T* mntr)
{
	mntr->pRxBuffer[mntr->Index++] = CH_STOP;
}

/**
 * @brief	Handle I2C1 interrupt by calling I2CM interrupt transfer handler
 * @return	Nothing
 */
void I2C1_IRQHandler(void)
{
	uint32_t data;

	if(i2c_GetPendingInt(LPC_I2C1) & I2C_INTSTAT_MONRDY)
	{
		if(i2c_GetStatus(LPC_I2C1) && I2C_STAT_MONRDY)	//data ready get data
		{
			data = i2c_GetMonitorRxData(LPC_I2C1);

			if(data & I2C_MONRXDAT_MONSTART)
			{
				i2cMonitor_PutStart(Monitor1);
			}

			if(data & I2C_MONRXDAT_MONRESTART)
			{
				i2cMonitor_PutRestart(Monitor1);
			}

			i2cMonitor_PutData(Monitor1, (uint8_t)data);

			if(data & I2C_MONRXDAT_MONNACK)
			{
				i2cMonitor_PutACK(Monitor1, CH_NACK);
			}
			else
			{
				i2cMonitor_PutACK(Monitor1, CH_ACK);
			}
		}
	}

	if(i2c_GetPendingInt(LPC_I2C1) & I2C_INTSTAT_MONIDLE )
	{

		i2c_ClearStatus(LPC_I2C1, I2C_STAT_MONIDLE);

		if(Monitor1->Index>0)
		{
			i2cMonitor_PutStop(Monitor1);

			Monitor1->Len = Monitor1->Index;
			Monitor1->DataReady = -1;
			Monitor1->Index = 0;
		}
	}

	if(i2c_GetPendingInt(LPC_I2C1) & I2C_INTSTAT_MONOV)
	{
		i2c_ClearStatus(LPC_I2C1, I2C_STAT_MONOV);
	}
}

void I2C0_IRQHandler(void)
{
	__asm volatile ("nop");
}

void I2C2_IRQHandler(void)
{
	__asm volatile ("nop");
}

/*************************************
 *  Public Functions
 *************************************/

void i2cmonitor_Start(I2C_MONITOR_T* mntr,LPC_I2C_T* i2c)
{
	Monitor1 = mntr;
	/* Enable I2C clock and reset I2C peripheral */
	Chip_I2C_Init(i2c);

	/* Clear interrupt status and enable monitor interrupts */
	Chip_I2C_EnableInt(i2c, I2C_INTENSET_MONIDLE | I2C_INTENSET_MONOV | I2C_INTENSET_MONRDY);

	/* Enable I2C monitor interface */
	i2c_MonitorEnable(i2c);

	if(i2c == LPC_I2C1)
	{
		/* Enable the interrupt for the I2C */
		NVIC_EnableIRQ(I2C1_IRQn);
	}
}




